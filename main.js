var Q = require("q");
var unirest = require("unirest");
var time = 1000; //time for requests (for not brute forcing skroutz servers)
var page = 1; //start searching from n page
var token = ''; //token initialization (will be grabbed in getToken function)
var category_id = 55; //category id for searching
var priceDifference = 0.6; //get products where price difference is 60% from first to second result
var client_secret = '' //client seret of the Skroutz RESTful api
var client_id = ''; //client secret of the Skroutz RESTful api

/**
  requests for token using credentials
**/
function getToken() {
    var deferred = Q.defer();

    var req = unirest("POST", "https://www.skroutz.gr/oauth2/token?client_id="+client_id+"&client_secret="+client_secret+"&grant_type=client_credentials&redirect_uri=http://example.com/auth/skroutz&scope=public");
    req.end(function(res) {
        if (res.error) throw new Error(res.error);
        token = res.body.access_token;
        console.log(token)
        var params = {
            "category_id": category_id,
            "token": token
        };

        deferred.resolve(params);
    });
    return deferred.promise;
}

/**
  get category with products
**/
function getSkus(params) {
    setInterval(function() {
        var url = "http://api.skroutz.gr/categories/" + params.category_id + "/skus?page=" + page + "&order_by=pricevat&order_dir=asc";
        var req = unirest("GET", url);
        req.headers({
            "Accept": "application/vnd.skroutz+json; version=3",
            "Authorization": "Bearer " + token
        });

        req.end(function(res) {
            var skus = res.body.skus;
            var arr = skus.map(function(item) {
                return item.id;
            });

            return Q.all(arr.map(function(sku_id) {
                var params = {
                    "sku_id": sku_id
                };
                return getSku(params);
            }));
        });
        page++;
    }, time)
}

/**
  get product info page
**/
function getSku(params) {
    var url = "http://api.skroutz.gr/skus/" + params.sku_id + "/products?per=2&order_by=pricevat&order_dir=asc";

    var req = unirest("GET", url);
    req.headers({
        "Accept": "application/vnd.skroutz+json; version=3",
        "Authorization": "Bearer " + token
    });

    req.end(function(res) {
        var pricesArr = res.body.products.map(function(product) {
            return product.price;
        })
        console.log(pricesArr);
        if (pricesArr.length > 1) {
            if (pricesArr[0] / pricesArr[1] < priceDifference) {
                console.log(res.body.products[0]);
                console.log(res.body.products[1]);
            }
        }
    });
}

getToken()
    .then(getSkus);
