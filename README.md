# Skroutz Deals Finder
## Description
###### Finds Skroutz deals using skroutz RESTful api. 

This script searches through a category for price differences between the first and the second store ordered by price. If the price difference is above a percentage, it prints the url to the console.
